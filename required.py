import random
import time
import os
import subprocess
# Windows Functions


def wait(waitTime):
    time.sleep(waitTime/1000)


def kill(ps):
    os.system("taskkill /f /t /im " + ps)


def KillCheck():
    wait(300)
    kill("emulator.exe")
    wait(5000)


def run(WhatToRun):
    subprocess.Popen(WhatToRun)

# ADB Functions  emulator-5554

class adb():
    def run(WhatToRun):
        subprocess.Popen("adb -s " + adbEmulatorId + " " + WhatToRun)

def runDroid(packageNameAndActivity):
    adb.run("shell am start -n " + packageNameAndActivity)

def killDroid(packageName):
    adb.run("shell am force-stop " + packageName)
    wait(1000)

def startup():
    run("adb kill-server")
    wait(5000)
    run("adb start-server")
    wait(5000)
    run("adb devices")